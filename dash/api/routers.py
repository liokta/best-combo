from django.conf.urls import url, include
from rest_framework import routers
from api.viewsets import UserViewSet
from rest_framework.authtoken import views as views_auth_token


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='api')),
    url(r'^api-token-auth/', views_auth_token.obtain_auth_token),
]

