from django.shortcuts import render

def home(request):

    context = {
        'title': 'Welcome',
    }

    return render(request, 'web/home.html', context)